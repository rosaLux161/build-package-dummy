#!/bin/bash

source venv/bin/activate
make
python3 -m build
python3 -m twine upload --repository gitlab --config-file .pypirc  dist/*
deactivate
python setup.py sdist bdist_wheel