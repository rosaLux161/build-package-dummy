import requests

def get(product):
    return requests.get(f'https://dummyjson.com/products/{product}').json()
