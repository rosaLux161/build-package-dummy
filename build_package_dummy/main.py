import chevron

from build_package_dummy import core

def main():
    # print(core.get_product(1))
    with open('build_package_dummy/template.mustache', 'r') as f:
        print(chevron.render(f, {'product': core.get_product(1)['product']['title']}))

if __name__ == "__main__":
    main()
