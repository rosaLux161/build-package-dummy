from . import helpers

def get_product(product):
    return {"product": helpers.get(product)}
